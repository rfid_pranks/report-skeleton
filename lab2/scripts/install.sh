#!/bin/bash

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

cd ${SCRIPTPATH}/..

insmod driver/build/vramdisk_driver.ko

for i in {1..3}
do
    mkfs.vfat /dev/vramdisk${i}
    mkdir /mnt/vramdisk${i}mp
    mount /dev/vramdisk${i} /mnt/vramdisk${i}mp
done
