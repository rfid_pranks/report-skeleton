#include <arpa/inet.h>
#include <fstream>
#include <iostream>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int main() {
    std::string msg = "aboba";
    struct sockaddr_in serAddr;
    serAddr.sin_family = AF_INET;
    serAddr.sin_port = htons(50001);
    serAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    int socketDescriptor;
    if ((socketDescriptor = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket creation error...\n");
        std::cout << "fail\n";
        exit(-1);
    }
    std::cout << "descriptor: " << socketDescriptor << std::endl;
    if (sendto(socketDescriptor, msg.data(), msg.length(), 0, (struct sockaddr *)&serAddr,
               sizeof(serAddr)) < 0) {
        std::cout << "fail\n";
        perror("sending error...\n");
        close(socketDescriptor);
        exit(-1);
    }
    std::cout << "sent " << msg.length() << "\n";
}
