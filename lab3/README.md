# Лабораторная работа 3

**Название:** "Разработка драйверов сетевых устройств"

**Цель работы:** получить знания и навыки разработки драйверов сетевых 
интерфейсов для операционной системы Linux.

## Описание функциональности драйвера
Драйвер создаёт сетевое устройство, подключаающееся к интерфейсу `lo` и анализирующее ip пакеты, которые на него приходят. При необходимости сетевой интерфейс может быть изменён при пересборке драйвера. Создаваемое сетевое устройство пеерехватывает ip пакеты, содержащие UDP диаграммы, порт назначения которых совпадает с целевым. Целевой порт настраивается

Помимо снятия статистики сетевого устройства, с драйвером можно взаиможействовать через файл `/proc/UDP_interceptor`.
При чтении из этого файла выводится статистика драйвера: количество просмотренных и перехваченных датаграмм, а также целевой порт.
Чтобы изменить целевой порт, его номер надо записать в файл `/proc/UDP_interceptor`.
При записи нового порта он валидируется, ошибки пишутся в `dmesg`.
Для перехваченных датаграмм в `dmesg` выводится подробная информация

## Инструкция по сборке
1. Перейти в папку driver
1. Выполнить `make`
1. Перейти в папку udp-client
1. Выполнить `make`

## Инструкция пользователя
1. После сборки драйвера выполнить команду `insmod` для получившегося модуля.
1. С помощью записи номера порта в файл `/proc/UDP_interceptor` установить целевой порт
1. Отправлять UDP датаграммы и смотреть результаты в `dmesg` и `/proc/UDP_interceptor`
1. По завершении работы извлечь модуль командой `rmmod virt_net_if`

## Примеры использования
```
$dmesg
...
[ 2559.045329] dest port: 13568
[ 2559.045330] We don't intercept it
[ 2559.045512] dest port: 15300
[ 2559.045513] We don't intercept it
[ 2562.812927] dest port: 20931
[ 2562.812930] We don't intercept it
[ 2569.517671] dest port: 20931
[ 2569.517675] We don't intercept it
[ 2570.187058] dest port: 20931
[ 2570.187071] We don't intercept it

$cat /proc/UDP_interceptor 
This module intercepts UDP datagrams sent to port 1488
Observed datagrams number: 7
Intercepted datagrams number: 0
You can assign new port to intercept datagrams by writing it to this file.

$echo 20931 >/proc/UDP_interceptor 
$ cat /proc/UDP_interceptor 
This module intercepts UDP datagrams sent to port 20931
Observed datagrams number: 7
Intercepted datagrams number: 0
You can assign new port to intercept datagrams by writing it to this file.

$ cat /proc/UDP_interceptor 
This module intercepts UDP datagrams sent to port 20931
Observed datagrams number: 8
Intercepted datagrams number: 1
You can assign new port to intercept datagrams by writing it to this file.

$ dmesg
...
[ 2708.942970] Captured UDP datagram, saddr: 127.0.0.1
[ 2708.942975] daddr: 127.0.0.1
[ 2708.942977] source port: 62338
[ 2708.942979] dest port: 20931
[ 2708.942980] Data length: 5. Data:
```


